KLAVIYO
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Future developments
 * Contributions are welcome!!
 * Maintainers


INTRODUCTION
------------

This module facilitates integration with email marketing platform Klaviyo.

Ideal for ecommerce and web-based sites dealing with a widespread target audience, 
this module helps users manage email lists efficiently. Count on this module to launch 
email marketing campaigns that add value to your business. 

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/klaviyo_list

REQUIREMENTS
------------

Entity API and Entity tokens modules must be enabled. A Klaviyo account is required.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

Provide the Klaviyo API Key by going to Configuration -> Web services -> Klaviyo.

CONTRIBUTIONS ARE WELCOME!!
---------------------------

Feel free to follow up in the issue queue at

https://www.drupal.org/project/issues/klaviyo_list

for any contributions, bug reports, feature requests.
Tests, feedback or comments in general are highly appreciated.


MAINTAINERS
-----------

Current maintainers:

 * Neerav Mehta (neeravbm) - https://www.drupal.org/u/neeravbm

This project has been sponsored by:
 * RED CRACKLE
   Specialized in consulting and planning of Drupal powered sites, RED CRACKLE
   offers installation, development, theming, customization, and hosting
   to get you started. Visit http://redcrackle.com for more information.
