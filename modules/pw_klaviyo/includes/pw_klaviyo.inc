<?php

/**
 * @file
 * klaviyo required function.
 */

/**
 * Welcome pages klaviyo form changes
 *
 * @param $form
 *  Form array
 * @param $account
 *  User account object
 */
function pw_klaviyo_welcome_message_klaviyo(&$form, $account) {
  module_load_include('inc', 'klaviyo', 'includes/klaviyo.user');
  module_load_include('inc', 'klaviyo_lists', 'includes/klaviyo_lists.user');
  $list_display = FALSE;
  $form['users'] = array(
    '#type' => 'value',
    '#value' => $account
  );
  $lists = klaviyo_lists_get_available_lists($account);
  if (!empty($lists)) {
    // Wrap in a fieldset.
    $form['klaviyo_lists'] = array(
      '#type' => 'fieldset',
      '#title' => t(''),
      '#tree' => TRUE,
    );
    foreach ($lists as $list) {
      if (user_exists_in_klaviyo($account->mail, $list->kl_list_id) == FALSE) {
        $list_display = TRUE;
        klaviyo_lists_auth_newsletter_form($form['klaviyo_lists'], $list, $account);
        $form['klaviyo_lists']['klaviyo_' . $list->name]['subscribe']['#title'] = t('Please consider signing for the ProgrammableWeb Today Newsletter to receive info on new APIs and related news.');
        $form['klaviyo_lists']['klaviyo_' . $list->name]['subscribe']['#ajax'] = array(
          'callback' => 'pw_klaviyo_ajax_subscriptions_callback_klaviyo',
          'wrapper' => 'edit-klaviyo-lists',
          'effect' => 'fade',
        );
      }
    }
    if ($list_display == FALSE) {
      unset($form['klaviyo_lists']);
    }
  }
}

/**
 * Callback Function
 * Ajax Callback for the klaviyo checkbox.
 */
function pw_klaviyo_ajax_subscriptions_callback_klaviyo($form, $form_state) {
  foreach ($form_state['values']['klaviyo_lists'] as $subscribe) {
    if ($subscribe['subscribe'] == 1) {
      $return_message = subscribe_klaviyo($form_state['values']['users'], $subscribe);
      drupal_set_message(check_plain($return_message), 'status');
      return theme('status_messages');
    }
    else {
      $return_message = unsubscribe_klaviyo($form_state['values']['users'], $subscribe);
      drupal_set_message(check_plain($return_message), 'status');
      return theme('status_messages');
    }
  }
}

/**
 * Dashboard klaviyo form changes
 *
 * @param $form
 *  Form array
 * @param $hide_subscription
 *  This is boolean which define is checkbox should show or not
 * @param $user
 *  Logged in user account object
 * @param $account
 *  User account object
 */
function _pw_dashboard_subscription_klaviyo(&$form, $hide_subscription, $user, $account) {
  module_load_include('inc', 'klaviyo_lists', 'includes/klaviyo_lists.user');
  if (module_exists('klaviyo_lists') && $hide_subscription == FALSE && $user->uid > 0) {
    $form['users'] = array('#type' => 'value', '#value' => $account);
    // Need to force feed the authenticated role to this account object so the right newsletters are available:
    $account->roles[DRUPAL_AUTHENTICATED_RID] = 'authenticated user';
    $lists = klaviyo_lists_get_available_lists($account);
    if (!empty($lists)) {
      // Wrap in a fieldset.
      $form['klaviyo_lists'] = array(
        '#type' => 'fieldset',
        '#title' => t(''),
        '#tree' => TRUE,
      );
      foreach ($lists as $list) {
        klaviyo_lists_auth_newsletter_form($form['klaviyo_lists'], $list, $account);
      }
    }
  }
  $form['klaviyo_lists']['klaviyo_programmableweb_today']['subscribe']['#title'] = 'Subscribe To Our Newsletter';
  $form['klaviyo_lists']['klaviyo_programmableweb_today']['subscribe']['#ajax'] = array(
    'callback' => 'pw_klaviyo_ajax_subscriptions_callback_klaviyo',
    'wrapper' => 'edit-klaviyo-lists',
    'effect' => 'fade',
  );
}