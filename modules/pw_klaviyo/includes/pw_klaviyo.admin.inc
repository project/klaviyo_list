<?php

/**
 * @file
 * Klaviyo module admin settings.
 */

/**
 * Return a form for adding/editing a klaviyo master list.
 */
function klaviyo_master_list_setting_form($form, &$form_state, KlaviyoList $list = NULL) {
  module_load_include('inc', 'klaviyo', 'includes/klaviyo.user');
  $form = array();
  $klaviyo_master_list = variable_get('klaviyo_master_list');
  $form['kl_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select Master List in Klaviyo'),
    '#collapsible' => TRUE,
    '#collapsed' => isset($list),
  );

  $lists = klaviyo_get_lists();
  $options = array('' => t('-- Select --'));
  foreach ($lists as $key => $kl_list) {
    $options[$kl_list['id']] = $kl_list['name'];
  }
  $form['kl_list']['klaviyo_master_list'] = array(
    '#type' => 'select',
    '#title' => t('Klaviyo Master List'),
    '#multiple' => FALSE,
    '#description' => t('Available Klaviyo lists. If this field is empty,
      create a list at !Klaviyo first.',
      array('!Klaviyo' => l(t('Klaviyo'), 'https://www.klaviyo.com/lists'))),
    '#options' => $options,
    '#default_value' => $klaviyo_master_list ? $klaviyo_master_list : -1,
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
