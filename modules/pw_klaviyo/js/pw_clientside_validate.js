(function ($) {
$("form#klaviyo-lists-user-subscribe-form").find( ".g-recaptcha" ).hide();
$('#edit-klaviyo-form-klaviyo-programmableweb-today-email').keydown(function(){
  // $('#block-klaviyo-lists-1-klaviyo button[type="submit"]').hide();
    $("form#klaviyo-lists-user-subscribe-form").find( "p.help-block" ).css({"display":"inline","width":"auto"});
   $("form#klaviyo-lists-user-subscribe-form").find( ".g-recaptcha" ).show();
});
$("#edit-klaviyo-form-submit").click(function(e){
  e.preventDefault();
  $emailValue = $('#edit-klaviyo-form-klaviyo-programmableweb-today-email').val();
 //Check if form exists
   if ( $( "#edit-klaviyo-form-submit" ).length) {
    // Check if  value is empty and valid
    if(($emailValue) && (pwEmailValidate($emailValue))) {
      // Check if email is of pattern [0-9]@yahoo.com
          if (!pwInvalidEmailValidate($emailValue)) {
            $("#klaviyo-lists-user-subscribe-form")[0].submit();
                }
            else {
              return false;
                }
              } 
              else {
                $('.messages.error.error-pwt').remove();
                $('#block-klaviyo-lists-1-klaviyo').append('<div class="messages error error-pwt">Please enter Valid Email Address</div>');
              }
      }
});
// Function to check for Spam Accounts from Yahoo Email ID
function pwInvalidEmailValidate(emailAddress){
        // Any email address with pattern 
        //var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
       // Email address with [0-9]@yahoo.com
       var expr = /^([0-9_\-\.][a-zA-Z0-9_\-\.]+)@(yahoo+)\.([a-zA-Z]{2,5})$/;
        return expr.test(emailAddress);
    }
//Function to check if Email Address is valid
    function pwEmailValidate(emailAddress) {
    var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
    return EMAIL_REGEXP.test(emailAddress)
}

})(jQuery);
