<?php

/**
 * @file
 * klaviyo_lists module admin settings.
 */

/**
 * Administrative display showing existing lists and allowing edits/adds.
 */
function klaviyo_lists_overview_page() {
  module_load_include('inc', 'klaviyo', 'includes/klaviyo.user');
  module_load_include('inc', 'klaviyo_lists', 'includes/klaviyo_lists.user');
  $lists = klaviyo_lists_load_multiple();
  $rows = array();

  foreach ($lists as $list) {
    $actions = array(
      l(t('Edit'), 'admin/config/services/klaviyo/lists/' . $list->id . '/edit'),
      l(t('Delete'), 'admin/config/services/klaviyo/lists/' . $list->id . '/delete'),
    );
    $rows[] = array(
      l($list->label(), 'admin/config/services/klaviyo/lists/' . $list->id . '/edit'),
      $list->description,
      implode(' | ', $actions),
    );
  }
  $table = array(
    'header' => array(
      t('Name'),
      t('Description'),
      t('Actions'),
    ),
    'rows' => $rows,
  );
  $kl_lists = klaviyo_get_lists();
  if (empty($kl_lists)) {
    drupal_set_message(check_plain(t('You don\'t have any lists configured in your Klaviyo account, (or you haven\'t configured your API key correctly on the Global Settings tab). Head over to Klaviyo and create some lists"'))
    );
  }
  else {
    $options = 'Currently Available Klaviyo lists:<i>';
    foreach ($kl_lists as $kl_list) {
      $options .= ' ' . $kl_list['name'] . ',';
    }
    $options = rtrim($options, ',');
    $options .= ".</i>";
    $table['caption'] = $options;
  }
  return theme('table', $table);
}

/**
 * Return a form for adding/editing a klaviyo list.
 */
function klaviyo_lists_list_form($form, &$form_state, KlaviyoList $list = NULL) {
  module_load_include('inc', 'klaviyo', 'includes/klaviyo.user');
  module_load_include('inc', 'klaviyo_lists', 'includes/klaviyo_lists.user');
  $form = array();

  // Store the existing list for updating on submit.
  if (isset($list)) {
    $form_state['list'] = $list;
  }

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('The label for this list that appears in the admin UI and the default Block Title.'),
    '#size' => 35,
    '#maxlength' => 32,
    '#default_value' => $list ? $list->label : '',
    '#required' => TRUE,
  );

  // Machine-readable list name.
  $status = isset($list->status) && $list->id && (($list->status & ENTITY_IN_CODE) || ($list->status & ENTITY_FIXED));
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($list->name) ? $list->name : '',
    '#maxlength' => 32,
    '#disabled' => $status,
    '#machine_name' => array(
      'exists' => 'klaviyo_lists_load_multiple_by_name',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this list. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#title' => 'Description',
    '#type' => 'textarea',
    '#default_value' => $list ? $list->description : '',
    '#rows' => 2,
    '#maxlength' => 255,
    '#description' => t('This description will be shown to the user on the list signup and user account settings pages. (255 characters or less)'),
  );

  $form['kl_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Klaviyo List & Merge Fields'),
    '#collapsible' => TRUE,
    '#collapsed' => isset($list),
  );
  unset($form['list_settings']['roles'][DRUPAL_ANONYMOUS_RID]);
  $form['list_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => TRUE,
  );

  $form['form_option_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form & Subscribe Block Options'),
  );
  $lists = klaviyo_get_lists();

  $options = array('' => t('-- Select --'));
  foreach ($lists as $key => $kl_list) {
    $options[$kl_list['id']] = $kl_list['name'];
  }
  $form['kl_list']['kl_list_id'] = array(
    '#type' => 'select',
    '#title' => t('Klaviyo List'),
    '#multiple' => FALSE,
    '#description' => t('Available Klaviyo lists. If this field is empty,
      create a list at !Klaviyo first.',
      array('!Klaviyo' => l(t('Klaviyo'), 'https://www.klaviyo.com/lists'))),
    '#options' => $options,
    '#default_value' => $list ? $list->kl_list_id : -1,
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'klaviyo_lists_mergefields_callback',
      'wrapper' => 'mergefields-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
      'progress' => array(
        'type' => 'throbber',
        'message' => t('Retrieving merge fields for this list.'),
      ),
    ),
  );

  $form['kl_list']['mergefields'] = array(
    '#prefix' => '<div id="mergefields-wrapper">',
    '#suffix' => '</div>',
  );

  // Show merge fields if changing list field or editing existing list.
  if (!empty($form_state['values']['kl_list_id']) || isset($list)) {

    $form['kl_list']['mergefields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Merge Fields'),
      '#id' => 'mergefields-wrapper',
      '#tree' => TRUE,
    );
    $kl_list = klaviyo_lists_get_field_list();

    if (isset($kl_list['mergevars']) && !empty($kl_list['mergevars'])) {
      foreach ($kl_list['mergevars'] as $mergevar) {
        $default_value = isset($list->settings['mergefields'][$mergevar['tag']]) ? $list->settings['mergefields'][$mergevar['tag']] : -1;
        $disabled = FALSE;
        $description = '';
        if ($mergevar['tag'] == 'EMAIL') {
          $default_value = 'mail';
          $disabled = TRUE;
          $description = t("Email is required and must map to a Drupal user's email.");
        }
        $form['kl_list']['mergefields'][$mergevar['tag']] = array(
          '#type' => 'select',
          '#title' => check_plain($mergevar['name']),
          '#description' => $description,
          '#default_value' => $default_value,
          '#disabled' => $disabled,
          '#required' => $mergevar['req'],
          '#options' => klaviyo_lists_get_merge_tokens(),
        );
      }
    }
    else {
      $form['kl_list']['mergefields']['message'] = array(
        '#markup' => t('There are no merge fields configured for this list.'),
      );
    }
  }

  // For clock setting
  $form['list_settings']['allow_anonymous'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow anonymous registrations. <em>(Formerly "Freeform List")</em>'),
    '#description' => t('This allows site visitors who do not have an account on your site to register for this list using a form block.'),
    '#default_value' => ($list && !empty($list->settings['allow_anonymous'])) ? $list->settings['allow_anonymous'] : FALSE,
  );

  $form['list_settings']['roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#tree' => TRUE,
    '#description' => t('<p>Choose which roles may subscribe to this list. All
      users will see the lists they\'re eligible for at the !subscribe and in
      the subscription block. Lists assigned to the Authenticated role may
      also apear in the registration form if that option is selected below.
      Authenticated user may also manage their list settings from their profile.
      The Anonymous role may <em>only</em> be set for free form lists.</p>',
      array(
        '!subscribe' => l(t('newsletter subscription page'),
          'user'),
      )),
  );
  $roles = user_roles();
  foreach ($roles as $rid => $role) {
    $form['list_settings']['roles'][$rid] = array(
      '#type' => 'checkbox',
      '#tree' => TRUE,
      '#title' => check_plain($role),
      '#return_value' => $rid,
      '#default_value' => ($list && !empty($list->settings['roles'][$rid])) ? $list->settings['roles'][$rid] : FALSE,
    );
  }
  unset($form['list_settings']['roles'][DRUPAL_ANONYMOUS_RID]);
  $form['list_settings']['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
  );

  $form['list_settings']['settings']['show_register_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show subscription options on the user registration form.'),
    '#description' => t('This will only apply for lists granted to an authenticated role. <em>(If the list is set to "Automatically add", only the titlewill appear.)</em>'),
    '#default_value' => isset($list->settings['show_register_form']) ? $list->settings['show_register_form'] : FALSE,
  );
  $form['list_settings']['settings']['show_account_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show subscription options on user edit screen'),
    '#description' => t('If set, a tab will be presented for managing newsletter subscriptions when editing an account. <em>(If the list is set to "Automatically add", the option to unsubscribe will be disabled.)</em>'),
    '#default_value' => isset($list->settings['show_account_form']) ? $list->settings['show_account_form'] : FALSE,
  );

  $form['form_option_settings']['form_label'] = array(
    '#type' => 'textfield',
    '#title' => t('List Label'),
    '#description' => t('The label for this list that will appear on forms, either next to a checkbox or above the list merge fields, depending on the type of list.'),
    '#size' => 40,
    '#maxlength' => 64,
    '#default_value' => isset($list->settings['form_label']) ? $list->settings['form_label'] : '',
  );
  $form['form_option_settings']['submit_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Submit Button Label'),
    '#description' => t('The label for the Submit button that will appear on subscription forms that include only this list: generally blocks only.'),
    '#size' => 20,
    '#maxlength' => 20,
    '#default_value' => isset($list->settings['submit_label']) ? $list->settings['submit_label'] : 'Submit',
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#access' => isset($list),
    '#submit' => array('Klaviyo_lists_list_delete_submit'),
  );
  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/services/Klaviyo/lists',
  );
  return $form;
}


/**
 * Return an array of available user tokens.
 */
function klaviyo_lists_get_merge_tokens() {
  $out = array('' => t('-- Select --'));

  // Invoke hook to get all merge tokens:
  $tokens = module_invoke_all('klaviyo_lists_merge_tokens');

  foreach ($tokens as $key => $token) {
    $out[$key] = t('!field', array('!field' => $token['name']));
  }
  return $out;
}

/**
 * Helper function used inside klaviyo_lists_list_form().
 *  This function is used for return list of fields
 * @return $kl_list
 *  This is an array of fields
 */
function klaviyo_lists_get_field_list() {
  $kl_list['mergevars'][0] = array(
    'name' => 'Email Address',
    'req' => 1,
    'field_type' => 'email',
    'public' => 1,
    'show' => 1,
    'order' => 0,
    'tag' => 'EMAIL',
    'id' => 0
  );
  $kl_list['mergevars'][1] = array(
    'name' => 'First Name',
    'req' => '',
    'field_type' => 'text',
    'public' => 1,
    'show' => 1,
    'order' => 1,
    'tag' => 'first_name',
    'id' => 1
  );

  $kl_list['mergevars'][2] = array(
    'name' => 'Last Name',
    'req' => '',
    'field_type' => 'text',
    'public' => 1,
    'show' => 1,
    'order' => 2,
    'tag' => 'last_name',
    'id' => 2
  );

  $kl_list['mergevars'][3] = array(
    'name' => 'Title',
    'req' => '',
    'field_type' => 'text',
    'public' => 1,
    'show' => 1,
    'order' => 3,
    'tag' => 'title',
    'id' => 3
  );
  $kl_list['mergevars'][4] = array(
    'name' => 'Company Name',
    'req' => '',
    'field_type' => 'text',
    'public' => 1,
    'show' => 1,
    'order' => 4,
    'tag' => 'organization',
    'id' => 4
  );

  $kl_list['mergevars'][6] = array(
    'name' => 'Twitter ID',
    'req' => '',
    'field_type' => 'text',
    'public' => 1,
    'show' => 1,
    'order' => 6,
    'tag' => 'TwitterID',
    'id' => 6
  );
  $kl_list['mergevars'][7] = array(
    'name' => 'LinkedIn',
    'req' => '',
    'field_type' => 'text',
    'public' => 1,
    'show' => 1,
    'order' => 7,
    'tag' => 'LinkedIn',
    'id' => 7
  );
  $kl_list['mergevars'][8] = array(
    'name' => 'Google',
    'req' => '',
    'field_type' => 'text',
    'public' => 1,
    'show' => 1,
    'order' => 8,
    'tag' => 'Google',
    'id' => 8
  );
  return $kl_list;
}

/**
 * AJAX callback to return fields for a given type.
 */
function klaviyo_lists_mergefields_callback($form, $form_state) {
  module_load_include('inc', 'klaviyo_lists', 'includes/klaviyo_lists.user');
  return $form['kl_list']['mergefields'];
}

/**
 * Validation handler for klaviyo_lists_list_form().
 */
function klaviyo_lists_list_form_validate($form, &$form_state) {

  // Required lists must have a role selected (other than anon).
  if (isset($form_state['values']['required']) && !empty($form_state['values']['required'])) {
    // Check if there was a role selected:
    $roles = array_filter($form_state['values']['roles']);
    if (empty($roles)) {
      form_set_error('roles',
        t('Required lists must have a role selected.'));
    }
  }

  // Ensure mail merge field is set correctly:
  if (!isset($form_state['values']['mergefields']['EMAIL']) ||
    $form_state['values']['mergefields']['EMAIL'] != 'mail'
  ) {
    form_set_error('EMAIL',
      t('The email merge field must be set to the user mail token.'));
  }
}

/**
 * Submit handler for klaviyo_lists_list_form().
 */
function klaviyo_lists_list_form_submit($form, &$form_state) {
  module_load_include('inc', 'klaviyo_lists', 'includes/klaviyo_lists.user');
  $list = isset($form_state['list']) ? $form_state['list'] : klaviyo_list_create();
  $is_new = isset($list->is_new) && $list->is_new;

  // Check for altered roles.
  $added_roles = FALSE;
  $removed_roles = FALSE;
  $form_state['values']['roles'][DRUPAL_ANONYMOUS_RID] = $form_state['values']['allow_anonymous'];
  if (!$is_new) {
    foreach ($form_state['values']['roles'] as $role_id => $role) {
      $was_on = !empty($form_state['list']->settings['roles'][$role_id]);
      $set_on = !empty($role);
      if ($was_on && !$set_on) {
        $removed_roles = TRUE;
      }
      if (!$was_on && $set_on) {
        $added_roles = TRUE;
      }
      if ($added_roles && $removed_roles) {
        break;
      }
    }
  }
  $list->kl_list_id = $form_state['values']['kl_list_id'];
  $list->label = $form_state['values']['label'];
  $list->name = $form_state['values']['name'];
  $list->description = $form_state['values']['description'];
  $list->settings = array(
    'roles' => array_filter($form_state['values']['roles']),
    'mergefields' => isset($form_state['values']['mergefields']) ? $form_state['values']['mergefields'] : NULL,
    'show_register_form' => $form_state['values']['show_register_form'],
    'show_account_form' => $form_state['values']['show_account_form'],
    'form_label' => $form_state['values']['form_label'],
    'allow_anonymous' => $form_state['values']['allow_anonymous'],
    'submit_label' => $form_state['values']['submit_label']
  );
  $ret = klaviyo_lists_save($list);
  if ($ret) {
    drupal_set_message(check_plain(t('List @name has been saved.',
      array('@name' => $list->label))));

    $form_state['redirect'] = 'admin/config/services/klaviyo/lists';
  }
  else {
    drupal_set_message(t(check_plain('There has been an error saving your list.')), 'error');
    watchdog("klaviyo error", 'There has been an error saving your list');
  }
}

/**
 * Submit function for the delete button on the menu item editing form.
 */
function klaviyo_lists_list_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/services/klaviyo/lists/' . $form_state['list']->id . '/delete';
}

/**
 * List deletion form.
 */
function klaviyo_lists_delete_list_form($form, &$form_state, $list) {
  $form_state['list'] = $list;
  return confirm_form($form,
    t('Are you sure you want to delete %name?', array('%name' => $list->label())),
    'admin/config/services/klaviyo/lists/' . $list->id . '/edit',
    t('This action cannot be undone, although it will not have any effect on the Klaviyo list.'),
    t('Delete list'));
}

/**
 * Submit handler for klaviyo_lists_delete_list_form().
 */
function klaviyo_lists_delete_list_form_submit($form, &$form_state) {
  module_load_include('inc', 'klaviyo_lists', 'includes/klaviyo_lists.user');
  $list = $form_state['list'];
  klaviyo_lists_delete_multiple(array($list->id));
  $delete_message = t($list->label() . ' has been deleted.');
  drupal_set_message(check_plain($delete_message));
  $form_state['redirect'] = 'admin/config/services/klaviyo/lists';
}

/**
 * Gets an array of all lists, keyed by the list name.
 *
 * @string $name
 *   If set, the list with the given name is returned.
 *
 * @return KlaviyoList[]
 *   Depending whether $name isset, an array of lists or a single one.
 */
function klaviyo_lists_load_multiple_by_name($name = NULL) {
  $lists = entity_load_multiple_by_name('klaviyo_list', isset($name) ? array($name) : FALSE);
  return isset($name) ? reset($lists) : $lists;
}
