<?php

/**
 * @file
 * klaviyo_lists module admin settings.
 */


/**
 * Deletes multiple lists by ID.
 *
 * @array $list_ids
 *   An array of contact IDs to delete.
 *
 * @return bool
 *   TRUE on success, FALSE otherwise.
 */
function klaviyo_lists_delete_multiple($list_ids) {
  return entity_get_controller('klaviyo_list')->delete($list_ids);
}

/**
 * Loads multiple registrations by ID or based on a set of matching conditions.
 *
 * @see entity_load()
 *
 * @array $list_ids
 *   Array of list IDS to load.
 * @array $conditions
 *   An array of conditions on the {klaviyo_list} table in the form
 *     'field' => $value.
 * @bool $reset
 *   Whether to reset the internal contact loading cache.
 *
 * @return array
 *   An array of contact objects indexed by registration_id.
 */
function klaviyo_lists_load_multiple($list_ids = array(), $conditions = array(), $reset = FALSE) {
  if (empty($list_ids)) {
    $list_ids = FALSE;
  }
  return entity_load('klaviyo_list', $list_ids, $conditions, $reset);
}

/**
 * Create a new Klaviyo List object.
 *
 * @param array $values
 *   Associative array of values. At least include array('type' => $type)
 *
 * @return KlaviyoList
 *   New KlaviyoList entity.
 */
function klaviyo_list_create(array $values = array()) {
  return entity_get_controller('klaviyo_list')->create($values);
}

/**
 * Saves a list.
 *
 * @KlaviyoList $list
 *   The full list object to save.
 *
 * @return KlaviyoList
 *   The saved list object.
 */
function klaviyo_lists_save(KlaviyoList $list) {
  return $list->save();
}

/**
 * Return a form element for a single newsletter.
 */
function klaviyo_lists_auth_newsletter_form(&$form, $list, $account) {
  module_load_include('inc', 'klaviyo', 'includes/klaviyo.user');
  // Determine if a user is subscribed to the list.
  $is_subscribed = TRUE;
  $default_subscribed = FALSE;
  $is_anonymous = !empty($account->roles[DRUPAL_ANONYMOUS_RID]) && empty($account->roles[DRUPAL_AUTHENTICATED_RID]);
  if ($account && $account->uid > 0) {
    $is_subscribed = user_exists_in_klaviyo($account->mail, $list->kl_list_id);
  }
  // Wrap in a div:
  $wrapper_key = 'klaviyo_' . $list->name;
  $form[$wrapper_key] = array(
    '#prefix' => '<div id="klaviyo-newsletter-' . $list->name . '" class="klaviyo-newsletter-wrapper">',
    '#suffix' => '</div>',
  );

  $form[$wrapper_key]['list'] = array('#type' => 'value', '#value' => $list);
  // Add the title and description to lists for anonymous users or if requested:
  $title = isset($list->settings['form_label']) && !empty($list->settings['form_label']) ? t($list->settings['form_label']) : t('Subscribe to the @newsletter newsletter', array('@newsletter' => $list->label));

  // Add merge vars for anonymous forms:
  if ($list->settings['allow_anonymous'] && $is_anonymous) {

    $form['klaviyo_form'][$wrapper_key]['email'] = klaviyo_lists_insert_drupal_form_tag($title);

  }
  else {
    $form[$wrapper_key]['subscribe'] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($title),
      '#default_value' => ($default_subscribed && !$account->uid) || $is_subscribed,
      '#description' => check_plain($list->description),
    );
  }

  return $form;
}

/**
 * Return all available lists for a given user.
 *
 * @param <drupal_user> $account
 *   Optional account to get available lists for. Otherwise returns a general
 *   list.
 * @param <array> $conditions
 *   List settings to filter the results by (uses logical "AND").
 *   Options include the following (see klaviyo_lists_list_form_submit()):
 *     'show_account_form' => boolean
 *     'show_register_form' => boolean
 *     'form_label' => string
 *     'submit_label' => string
 *
 * @return <array>
 *   An array of appropriate klaviyo_list objects.
 */
function klaviyo_lists_get_available_lists($account = NULL, $conditions = array()) {
  if (empty($account)) {
    $user = $GLOBALS['user'];
    $account = $user;
  }
  $lists = klaviyo_lists_load_multiple(array());

  $user_lists = array();
  foreach ($lists as $lid => $list) {
    foreach ($account->roles as $rid => $role) {
      if (isset($list->settings['roles'][$rid]) && $list->settings['roles'][$rid]) {
        $pass = TRUE;
        if (!empty($conditions)) {
          foreach ($conditions as $key => $condition) {
            if (!isset($list->settings[$key]) || $list->settings[$key] != $condition) {
              $pass = FALSE;
            }
          }
        }
        if ($pass) {
          $user_lists[$lid] = $list;
        }
        break;
      }
    }
  }
  return $user_lists;
}

/**
 * Returns a subscription form, or forms, for a given user as a single form.
 *
 * If there are multiple lists, this generates a single form for all of them.
 */
function klaviyo_lists_user_subscribe_form($form, &$form_state, $lists, $account) {
  module_load_include('inc', 'klaviyo_lists', 'includes/klaviyo_lists.user');
  $form['#attributes'] = array('class' => array('klaviyo-lists-user-subscribe-form'));

  $form['klaviyo_form'] = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#tree' => TRUE,
  );
  foreach ($lists as $list) {
    $wrapper_key = 'klaviyo_' . $list->name;
    $form['klaviyo_form'][$wrapper_key]['list'] = array(
      '#type' => 'value',
      '#value' => $list
    );
    $form['klaviyo_form'][$wrapper_key]['email'] = klaviyo_lists_insert_drupal_form_tag('Email');
  }
  $submit_label = 'Subscribe';
  if (isset($lists[0]->settings['submit_label'])) {
    $submit_label = $lists[0]->settings['submit_label'];
  }

  $form['klaviyo_form']['submit'] = array(
    '#type' => 'submit',
    '#value' => t($submit_label),
    '#ajax' => array(
      'callback' => 'klaviyo_lists_user_subscribe_form_submit_ajax',
      'wrapper' => 'edit-klaviyo-form',
      'effect' => 'fade',
    )
  );
  return $form;
}

/**
 * Convert klaviyo form elements to Drupal Form API.
 *
 * @param  $mergevar
 *   This is title of field
 *
 * @return <drupal_form_element>
 *   A properly formatted drupal form element.
 */
function klaviyo_lists_insert_drupal_form_tag($mergevar) {
  // Insert common FormAPI properties:
  $input['#title'] = t('@mergevar', array('@mergevar' => $mergevar));
  $input['#type'] = 'textfield';
  $input['#element_validate'][] = 'klaviyo_lists_insert_drupal_form_tag_validate';
  $input['#attributes']['placeholder'] = t('Email Address');

  return $input;
}
