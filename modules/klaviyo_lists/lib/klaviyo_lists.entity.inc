<?php

/**
 * @file
 * klaviyo_lists module admin settings.
 */
class KlaviyoList extends Entity {
  public
    $id,
    $name,
    $kl_list_id,
    $label,
    $description,
    $settings;

  /**
   * Override parent constructor to set the entity type.
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, 'klaviyo_list');
  }

  /**
   * Return a label for a list.
   */
  public function label() {
    return $this->label;
  }

}
