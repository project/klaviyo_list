<?php

/**
 * @file
 * Install, update and uninstall functions for the klaviyo_lists module.
 *
 */

/**
 * Implements hook_schema().
 */
function klaviyo_lists_schema() {
  $schema['klaviyo_lists'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique klaviyo_list entity ID.',
      ),
      'name' => array(
        'description' => 'The machine-readable name of this klaviyo_list type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'kl_list_id' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
        'description' => 'The Klaviyo list id associated with this list.',
      ),
      'label' => array(
        'type' => 'varchar',
        'length' => 32,
        'description' => 'The {klaviyo_lists}.label of this klaviyo_list.',
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => 255,
        'description' => 'The {klaviyo_lists}.description of this klaviyo_list.',
        'not null' => FALSE,
        'default' => '',
      ),
      'settings' => array(
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized object that stores the settings for the specific list.',
      ),
      'status' => array(
        'type' => 'int',
        'not null' => TRUE,
        // Set the default to ENTITY_CUSTOM without using the constant as it is
        // not safe to use it at this point.
        'default' => 0x01,
        'size' => 'tiny',
        'description' => 'The exportable status of the entity.',
      ),
      'module' => array(
        'description' => 'The name of the providing module if the entity has been defined in code.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'unique key' => array('name'),
  );
  $schema['klaviyo_subscriber_user'] = array(
      'fields' => array(
        'id' => array(
          'type' => 'serial',
          'not null' => TRUE,
          'description' => 'Primary Key: Unique ID.',
        ),
        'drupal_uid' => array(
          'description' => 'Drupal user ID',
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
        ),
        'klaviyo_id' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => 32,
          'description' => 'The Klaviyo user id.',
        )
      ),
      'primary key' => array('id')
    );

  return $schema;
}

