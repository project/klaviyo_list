<?php

/**
 * @file
 * klaviyo required function.
 */

/**
 * Return all available lists for a given user.
 *
 * @param $account
 *   Optional account to get available lists for. Otherwise returns a general
 *   list.
 * @param $conditions
 *   List settings to filter the results by (uses logical "AND").
 *   Options include the following (see klaviyo_lists_list_form_submit()):
 *     'show_account_form' => boolean
 *     'show_register_form' => boolean
 *     'form_label' => string
 *     'submit_label' => string
 *
 * @return array
 *   An array of appropriate klaviyo_list objects.
 */
function klaviyo_get_available_lists($account = NULL, $conditions = array()) {
  module_load_include('inc', 'klaviyo_lists', 'includes/klaviyo_lists.user');
  $user = $GLOBALS['user'];
  if (empty($account)) {
    $account = $user;
  }
  $lists = klaviyo_lists_load_multiple(array());
  $user_lists = array();
  if (count($lists) > 0) {
    foreach ($lists as $lid => $list) {
      foreach ($account->roles as $rid => $role) {
        if (isset($list->settings['roles'][$rid]) && $list->settings['roles'][$rid]) {
          $pass = TRUE;
          if (!empty($conditions)) {
            foreach ($conditions as $key => $condition) {
              if (!isset($list->settings[$key]) || $list->settings[$key] != $condition) {
                $pass = FALSE;
              }
            }
          }
          if ($pass) {
            $user_lists[$lid] = $list;
          }
          break;
        }
      }
    }
  }
  return $user_lists;
}


/**
 * Return a form element for a single newsletter.
 * @param $form
 *  This is newsletter form
 * @param $list
 *  This is Klaviyo list Id
 * @param $account
 *  This is user account information
 * @return mixed
 *  This return form
 */
function klaviyo_auth_newsletter_form(&$form, $form_state, $list, $account) {
  // Determine if a user is subscribed to the list.
  $is_subscribed = user_exists_in_klaviyo($account->mail, $list->kl_list_id);
  // Wrap in a div:
  $wrapper_key = 'klaviyo_' . $list->name;
  $form[$wrapper_key] = array(
    '#prefix' => '<div id="klaviyo-newsletter-' . $list->name . '" class="klaviyo-newsletter-wrapper">',
    '#suffix' => '</div>',
  );
  $form[$wrapper_key]['list'] = array('#type' => 'value', '#value' => $list);

  $form[$wrapper_key]['subscribe'] = array(
    '#type' => 'checkbox',
    '#title' => check_plain(isset($list->settings['form_label']) && !empty($list->settings['form_label']) ? t($list->settings['form_label']) : t('Subscribe to the @newsletter newsletter', array('@newsletter' => $list->label()))),
    '#default_value' => $is_subscribed,
    '#description' => check_plain($list->description),
  );
  return $form;
}

/**
 * This function return list of lists from klaviyo
 * @return mixed
 *   This will return output of klaviyo_get_api_lists() function that will be an array of lists
 */
function klaviyo_get_lists() {
  module_load_include('inc', 'klaviyo', 'includes/klaviyo_class');
  return Klaviyo::klaviyo_get_api_lists();
}

/**
 * This function Sync user for subscribe or unsubscribe form Klaviyo list
 * @param $account
 *  This is user account information
 * @param $edit
 *  This is an array of user edit account information
 *
 */
function klaviyo_lists_user_sync($account, $edit) {
  if (isset($account->is_new) && !empty($account->is_new)) {
    foreach ($account->klaviyo_lists as $subscribe) {
      if ($subscribe['subscribe'] == 1) {
        subscribe_klaviyo($account, $subscribe);
      }
      else {
        // Checking if user already subscribe or not
        $subscribe_status = user_exists_in_klaviyo($account->mail, $subscribe['list']->kl_list_id);
        if ($subscribe_status == TRUE) {
          unsubscribe_klaviyo($account, $subscribe);
        }
      }
    }
  }
  else {
    foreach ($edit['klaviyo_lists'] as $edit_subscribe) {
      if ($edit_subscribe['subscribe'] == 1) {
        subscribe_klaviyo($account, $edit_subscribe);
      }
      else {
        // Checking if user already subscribe or not
        $subscribe_status = user_exists_in_klaviyo($account->mail, $edit_subscribe['list']->kl_list_id);
        if ($subscribe_status == TRUE) {
          unsubscribe_klaviyo($account, $edit_subscribe);
        }
      }
    }
  }
}

/**
 * This function is used for update klaviyo information
 * @param $account
 *  This is an array of user information
 * @param $edit
 *  This is list information (in which user will subscribe)
 * @param $klaviyo_id
 *  This is user's klaviyo ID
 * @param $data_array
 *  This is additional klaviyo profile parameter
 * @return string
 *  This function will return message if successfully update then success message else error
 */
function klaviyo_lists_user_sync_update($account, $edit, $klaviyo_id, $data_array = FALSE) {
  $return = Klaviyo::update_klaviyo($account, $edit, $klaviyo_id, $data_array);
  return $return;
}

/**
 * Subscribe user in klaviyo lists
 * @param $account
 *  This is user account information
 * @param $edit
 *  This is an array of user edit account information
 * @return string
 *  This function will return klaviyo subscribe response message
 *  if user subscribe successfully then it will return success message otherwise
 *  error message
 */
function subscribe_klaviyo($account, $edit) {
  module_load_include('inc', 'klaviyo', 'includes/klaviyo_class');
  $return = Klaviyo::subscribe_klaviyo_list($account, $edit);
  return $return;
}

/**
 * Unsubscribe user from klaviyo lists
 * @param $account
 *  This is user account information
 * @param $edit
 *   This is an array of user edit account information
 * @return string
 *  This function will return klaviyo unsubscribe response message
 *  If unsubscribe successfully then it will return success message otherwise it will return failed message
 */
function unsubscribe_klaviyo($account, $edit) {
  module_load_include('inc', 'klaviyo', 'includes/klaviyo_class');
  $return = Klaviyo::unsubscribe_klaviyo_list($account, $edit);
  return $return;
}

/**
 * Check if user already member of list
 * @param $mail
 *  User email address
 * @param $list_id
 *  klaviyo List id
 * @param $return_response
 *  This is optional parameter if this is tru then function will return user data from klaviyo response
 * @return bool
 *  If user in klaviyo and optional parameter is true then it will return klaviyo response data
 *  if optional parameter is not passed then it will return only true or false if user exist then true else false
 */
function user_exists_in_klaviyo($mail, $list_id, $return_response = FALSE) {
  module_load_include('inc', 'klaviyo', 'includes/klaviyo_class');
  return Klaviyo::user_exists_in_klaviyo_list($mail, $list_id, $return_response);
}

/**
 * This function used for delete klaviyo drupal relation entry from table
 * @param $uid
 *  This is user id
 * @return bool
 *  this will pass true if deleted
 */
function delete_klaviyo_relation($uid) {
  db_delete('klaviyo_subscriber_user')
    ->condition('drupal_uid', $uid)
    ->execute();
  return TRUE;
}

/**
 * This is helper function used for get merged fields value
 * @param $account
 *  This is user an array of user account information
 * @param $list
 *  This is an array of list information
 * @return $values
 *  This is an array of all merged fields key and value
 */
function klaviyo_lists_load_user_mergevars($account, $list) {
  $values = array();

  // Grab the saved list merge vars and filter out unset values:
  if (!empty($list->settings['mergefields'])) {
    $merge_vars = array_filter($list->settings['mergefields']);
    // We have to filter one-by-one, since an array_flip would munch any
    // duplicate values.
    foreach ($merge_vars as $name => $token) {
      // Match with token values:
      $replaced = module_invoke_all('klaviyo_lists_merge_values', array($token => $name), $account);
      if (isset($replaced[$name]) && !empty($replaced[$name])) {
        $values[$name] = strip_tags($replaced[$name]);
      }
      else {
        $values[$name] = '';
      }
    }
    // Always add email:
    $values += array(
      'EMAIL' => $account->mail,
    );
  }

  return $values;
}

/**
 * This function return user's klaviyo id
 * @param uid
 *  This is user's id
 * @return $uid
 *  If user's klaviyo id is find it return that otherwise return FALSE
 */
function get_klaviyo_id($uid) {
  $result = db_select('klaviyo_subscriber_user', 'ksu')
    ->fields('ksu', array('klaviyo_id'))
    ->condition('drupal_uid', $uid, '=')
    ->execute()
    ->fetchAssoc();
  // if klaviyo relation not save with given user then check if exist with another or not
  if (!empty($result['klaviyo_id'])) {
    return $result['klaviyo_id'];
  }
  else {
    return FALSE;
  }
}

/**
 * This function return list details from klaviyo
 * @return $record_array
 * This is an array of list details
 */
function klaviyo_get_api_list_data($list_id) {
  module_load_include('inc', 'klaviyo', 'includes/klaviyo_class');
  return Klaviyo::klaviyo_get_api_list_data_result($list_id);
}
