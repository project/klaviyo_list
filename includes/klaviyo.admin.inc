<?php

/**
 * @file
 * Klaviyo module admin settings.
 */

/**
 * Return the Klaviyo global settings form.
 */
function klaviyo_admin_settings() {
  $form['klaviyo_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('klaviyo API Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('klaviyo_api_key'),
    '#description' => t('The API key for your Klaviyo account. Get or generate a valid API key at your !apilink.', array('!apilink' => l(t('Klaviyo API Dashboard'), 'https://www.klaviyo.com/docs/api/lists'))),
  );
  return system_settings_form($form);
}
