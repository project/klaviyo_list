<?php

/**
 * @file
 * klaviyo Klaviyo and required functions
 */

/**
 * Class Klaviyo
 */
class Klaviyo {
  /**
   * Subscribe to klaviyo list
   * @param $account
   *  This is an array of user information
   * @param $subscribe
   *  This is an array of list information which is checked by user
   * @return $return_message
   *  This is return message if subscribe then success iff not then unsuccess
   */
  public function subscribe_klaviyo_list($account, $subscribe) {
    module_load_include('inc', 'klaviyo', 'includes/klaviyo.user');
    $user = $GLOBALS['user'];
    $api_key = variable_get('klaviyo_api_key');
    $field_data = klaviyo_lists_load_user_mergevars($account, $subscribe['list']);
    $field_data = self::get_klaviyo_field_array($field_data);
    $data = $field_data['fill_value'];
    $data['$unset'] = $field_data['unset_value'];
    $data['$ip'] = $user->hostname;
    $data = json_encode($data);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, 'email= ' . $account->mail . '&properties=' . $data . '&confirm_optin=false');
    curl_setopt($curl, CURLOPT_URL, 'https://a.klaviyo.com/api/v1/list/' . $subscribe['list']->kl_list_id . '/members?api_key=' . $api_key);
    $response = curl_exec($curl);
    curl_close($curl);
    $data = json_decode($response);
    $return_message = '';

    // If person id in response is not empty then check is relationship already save or not
    // else checking if user id is not empty (that anonymous user subscribe) then save relationship
    if (!empty($data->person->id)) {
      $relation_exist = self::check_exist_klaviyo_relation($account, $data->person->id);
      if ($relation_exist != FALSE && $relation_exist != 'exist') {
        watchdog("klaviyo error", $relation_exist);
      }
      elseif (isset($account->uid) && !empty($account->uid)) {
        // Insert in klaviyo_subscriber_user
        $insert_id = db_insert('klaviyo_subscriber_user')
          ->fields(array(
            'drupal_uid' => $account->uid,
            'klaviyo_id' => $data->person->id,
          ))
          ->execute();
        if (empty($insert_id)) {
          $return_message = $account->uid . 'user is Subscribe successfully but There is some problem in maintain Programmable Today record in site, kindly contact to site administrator.';
          watchdog("klaviyo error", $return_message);
        }
      }
    }
    if (empty($return_message) && isset($subscribe['list']->label) && !empty($subscribe['list']->label) && $data->status != 400 && !empty($data->person->id)) {
      $return_message = 'You have subscribed to ' . $subscribe['list']->label . '.';
    }
    elseif (empty($data->person->id) && empty($return_message)) {
      $return_message = 'There is some problem in subscribe klaviyo, Please contact to site administrator.';
      watchdog("klaviyo error", $account->uid . ' not subscribe in klaviyo return response  ' . $data->status);
      watchdog("klaviyo error", $return_message);
    }
    return $return_message;
  }

  /**
   * This function is used for update klaviyo information
   * @param $account
   *  This is an array of user information
   * @param $subscribe
   *  This is list information (in which user will subscribe)
   * @param $klaviyo_id
   *  This is user's klaviyo ID
   * @param $data_array_arg
   *  This is optional parameter used for user profile custom field in klaviyo
   * @return string
   *  This function will return message if successfully update thn success message else error
   */
  public function update_klaviyo($account, $subscribe, $klaviyo_id, $data_array_arg = FALSE) {
    module_load_include('inc', 'klaviyo', 'includes/klaviyo.user');
    $field_data = klaviyo_lists_load_user_mergevars($account, $subscribe['list']);
    $field_data = self::get_klaviyo_field_array($field_data);
    $data_array = $field_data['fill_value'];
    if ($data_array_arg != FALSE) {
      $data_array = $data_array_arg;
    }
    $data_array['$unset'] = json_encode($field_data['unset_value']);
    $api_key = variable_get('klaviyo_api_key');
    $data_array['email'] = $account->mail;
    $data_array['api_key'] = $api_key;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://a.klaviyo.com/api/v1/person/" . $klaviyo_id);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); // note the PUT here

    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data_array));
    $output = curl_exec($ch);
    $data = json_decode($output);
    if (isset($data->status) && $data->status == 400) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Unsubscribe user from klaviyo
   * @param $account
   *  This is an array of user information
   * @param $subscribe
   *  This is an array of list information which is checked by user
   */
  public function unsubscribe_klaviyo_list($account, $subscribe) {
    $api_key = variable_get('klaviyo_api_key');
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, 'email= ' . $account->mail . '&reason=unsubscribe');
    curl_setopt($curl, CURLOPT_URL, 'https://a.klaviyo.com/api/v1/list/' . $subscribe['list']->kl_list_id . '/members/exclude?api_key=' . $api_key);
    $response = curl_exec($curl);
    curl_close($curl);
    $data = json_decode($response);
    $return_message = '';
    if (!empty($data->num_excluded) || !empty($data->already_excluded)) {
      $return_message = 'You are Unsubscribe successfully.';
      drupal_set_message(check_plain(t($return_message)), 'status', FALSE);
    }
    elseif (isset($data->message) && !empty($data->message)) {
      $return_message = $data->message . '.';
      watchdog("klaviyo error", $return_message);
    }
    drupal_set_message(check_plain(t($return_message)), 'status', FALSE);
    return $return_message;
  }

  /**
   * This function is checking is user already subscribe or not in given list
   * @param $email
   *  This is user email
   * @param $list_id
   *  This is klaviyo list Id
   * @param $return_response
   *  This is boolean which define is return all information of klaviyo or just member or not
   * @return bool
   *  If user in klaviyo and optional parameter is true then it will return klaviyo response data
   *  if optional parameter is not passed then it will return only true or false if user exist then true else false
   */
  public static function user_exists_in_klaviyo_list($email, $list_id, $return_response = FALSE) {
    $api_key = variable_get('klaviyo_api_key');
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_URL, 'https://a.klaviyo.com/api/v1/list/' . $list_id . '/members?api_key=' . $api_key . '&email=' . $email);
    $response = curl_exec($curl);

    $data = json_decode($response);
    curl_close($curl);
    if (!empty($data->data) && $return_response == FALSE) {
      return TRUE;
    }
    elseif (!empty($data->data) && $return_response == TRUE) {
      $response = array(
        'is_subscribe' => TRUE,
        'data' => $data
      );
      return $response;
    }
    else {
      return FALSE;
    }
  }

  /**
   * This function return klaviyo list for current api key
   * @return $record_array
   *  This is an array of lists
   */
  public function klaviyo_get_api_lists() {
    $curl = curl_init();
    $api_key = variable_get('klaviyo_api_key');
    curl_setopt_array($curl, array(
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => 'https://a.klaviyo.com/api/v1/lists?api_key=' . $api_key,
    ));
    $response = curl_exec($curl);
    $data = json_decode($response);
    $records = $data->{'data'};
    $record_array = array();
    foreach ($records as $key => $record) {
      $record_array[$key]['id'] = $record->id;
      $record_array[$key]['name'] = $record->name;
      $record_array[$key]['person_count'] = $record->person_count;
    }
    curl_close($curl);
    return $record_array;
  }

  /**
   * This function is checking if user's relationship with klaviyo save already or not
   * If klaviyo's  relationship save with another user then it return error message
   * if klaviyo relationship save with same user then return exist
   * if klaviyo relation not save with any one then return false
   * @param $account
   *  This is user's account information
   * @param $subscribe
   *  This is list information (in which user want to subscribe)
   * @param $member_klaviyo
   *  This is klaviyo information
   * @return bool|string
   */
  public function check_exist_klaviyo_relation($account, $member_klaviyo_id) {
    $result = db_select('klaviyo_subscriber_user', 'ksu')
      ->fields('ksu', array('drupal_uid', 'klaviyo_id'))
      ->condition('klaviyo_id', $member_klaviyo_id, '=')
      ->execute()
      ->fetchAssoc();
    // if klaviyo relation not save with given user then check if exist with another or not
    if (!empty($result)) {
      if ($result['drupal_uid'] == $account->uid) {
        return 'exist';
      }
      else {
        $return_message = 'You are not able to subscribe,  Please contact support.';
        return $return_message;
      }
    }
    else {
      return FALSE;
    }
  }
  /**
   * This function is used for get all fields value is it empty or not
   * @param $fields
   *  This is list of fields
   * @return mixed
   * This will return an array which contain empty and fill fields
   */
  public function get_klaviyo_field_array($fields) {
    $field_array = array();
    $data_unset = array();
    // unset email field from custom user properties
    unset($fields['mail']);
    unset($fields['EMAIL']);
    foreach ($fields as $key => $field) {
      if (!empty($field)) {
        $field_array[$key] = $field;
      }
      else {
        $data_unset[] = $key;
      }
    }
    $user_fields['fill_value'] = $field_array;
    $user_fields['unset_value'] = $data_unset;
    return $user_fields;
  }

  /**
   * This function return klaviyo list for current list ID
   * @return $record_array
   *  This is an array of lists ID result
   */
  public function klaviyo_get_api_list_data_result($list_id) {
    $curl = curl_init();
    $api_key = variable_get('klaviyo_api_key');
    curl_setopt_array($curl, array(
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => 'https://a.klaviyo.com/api/v1/list/' . $list_id . '?api_key=' . $api_key,
    ));
    $response = curl_exec($curl);
    $record_array = json_decode($response);

    curl_close($curl);
    return $record_array;
  }
}
